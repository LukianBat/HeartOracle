package com.memebattle.heartoracle

import android.app.Application
import com.memebattle.heartoracle.core.di.AppComponent
import com.memebattle.heartoracle.core.di.DaggerAppComponent
import com.memebattle.heartoracle.core.di.module.BluetoothModule

class App : Application() {

    companion object {
        lateinit var instance: App
        lateinit var component: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        component = DaggerAppComponent.builder().bluetoothModule(BluetoothModule()).build()
    }
}