package com.memebattle.heartoracle.feature.base.scan.domain

import android.bluetooth.BluetoothDevice



interface ItemCallback {
    fun onItemClick(bluetoothDevice: BluetoothDevice)
}