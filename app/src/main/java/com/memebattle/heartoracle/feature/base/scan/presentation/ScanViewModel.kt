package com.memebattle.heartoracle.feature.base.scan.presentation

import androidx.lifecycle.ViewModel
import com.memebattle.heartoracle.App
import com.memebattle.heartoracle.core.domain.BaseCallback
import com.memebattle.heartoracle.core.domain.BluetoothConnectService
import com.memebattle.heartoracle.core.domain.BluetoothService
import com.memebattle.heartoracle.core.domain.EmptyCallback
import com.memebattle.heartoracle.feature.base.scan.domain.model.RecyclerDevices
import javax.inject.Inject

class ScanViewModel : ViewModel() {
    @Inject
    lateinit var bluetoothConnectService: BluetoothConnectService

    init {
        App.component.inject(this)
//        BluetoothService.turnOn(object : EmptyCallback {
//            override fun onSuccess() {
//
//            }
//
//            override fun onError(error: Throwable) {
//
//            }
//        })

    }

    fun scanDevices(callback: BaseCallback<ArrayList<RecyclerDevices>>) {
        bluetoothConnectService.searchDevice(object : BaseCallback<ArrayList<RecyclerDevices>> {
            override fun onSuccess(result: ArrayList<RecyclerDevices>) {
                callback.onSuccess(result)
            }

            override fun onError(error: Throwable) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

        })
    }

}