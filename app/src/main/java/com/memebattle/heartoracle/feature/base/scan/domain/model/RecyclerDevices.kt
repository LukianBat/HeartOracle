package com.memebattle.heartoracle.feature.base.scan.domain.model

import android.bluetooth.BluetoothDevice

data class RecyclerDevices(val name : String, val address : String, val bluetoothDevice: BluetoothDevice) {
}