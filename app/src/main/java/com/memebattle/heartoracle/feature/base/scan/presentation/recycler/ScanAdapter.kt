package com.memebattle.heartoracle.feature.base.scan.presentation.recycler

import android.bluetooth.BluetoothDevice
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.LayoutInflater
import androidx.recyclerview.widget.DiffUtil
import com.memebattle.heartoracle.feature.base.scan.domain.model.RecyclerDevices
import com.memebattle.heartoracle.R
import com.memebattle.heartoracle.feature.base.scan.domain.ItemCallback
import kotlinx.android.synthetic.main.device_item.view.*


class ScanAdapter(private val recyclerDevices: List<RecyclerDevices>, private val itemCallback : ItemCallback) : RecyclerView.Adapter<ScanAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScanAdapter.MyViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.device_item, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return recyclerDevices.size
    }

    override fun onBindViewHolder(holder: ScanAdapter.MyViewHolder, position: Int) {
        val btDevice = recyclerDevices[position]
        val itemView = holder.itemView
        itemView.itemDeviceName.text = btDevice.name
        itemView.itemDeviceAddress.text = btDevice.address
        itemView.setOnClickListener {
            itemCallback.onItemClick(btDevice.bluetoothDevice)
        }

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}