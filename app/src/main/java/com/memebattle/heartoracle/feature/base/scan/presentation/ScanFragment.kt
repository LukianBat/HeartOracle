package com.memebattle.heartoracle.feature.base.scan.presentation


import android.bluetooth.BluetoothDevice
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.memebattle.heartoracle.App
import com.memebattle.heartoracle.R
import com.memebattle.heartoracle.core.domain.BaseCallback
import com.memebattle.heartoracle.core.domain.BluetoothConnectService
import com.memebattle.heartoracle.feature.base.scan.domain.model.RecyclerDevices
import kotlinx.android.synthetic.main.fragment_scan.*
import javax.inject.Inject
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.LinearLayoutManager
import com.memebattle.heartoracle.feature.base.scan.domain.ItemCallback
import com.memebattle.heartoracle.feature.base.scan.presentation.recycler.ScanAdapter


class ScanFragment : Fragment() {


    lateinit var viewModel: ScanViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        viewModel = ViewModelProviders.of(this).get(ScanViewModel::class.java)
        return inflater.inflate(R.layout.fragment_scan, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        scanDevices()
    }

    fun scanDevices() {
        viewModel.scanDevices(object : BaseCallback<ArrayList<RecyclerDevices>> {
            override fun onSuccess(result: ArrayList<RecyclerDevices>) {
                val adapter = ScanAdapter(result, object : ItemCallback {
                    override fun onItemClick(bluetoothDevice: BluetoothDevice) {
                        Log.i("TAG", "click!!!!!")
                    }
                })
                val linearLayoutManager = LinearLayoutManager(context)
                val itemAnimator = DefaultItemAnimator()
                recycler.adapter = adapter
                recycler.layoutManager = linearLayoutManager
                recycler.itemAnimator = itemAnimator
            }

            override fun onError(error: Throwable) {

            }
        })
    }

}