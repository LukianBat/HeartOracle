package com.memebattle.heartoracle.core.domain

interface BaseCallback<T> {
    fun onSuccess(result: T)
    fun onError(error: Throwable)
}