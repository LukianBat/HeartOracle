package com.memebattle.heartoracle.core.domain

interface EmptyCallback {
    fun onSuccess()
    fun onError(error: Throwable)
}