package com.memebattle.heartoracle.core.domain

import com.memebattle.heartoracle.App
import io.reactivex.disposables.Disposable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import android.bluetooth.BluetoothSocket
import io.reactivex.SingleObserver
import io.reactivex.SingleEmitter
import android.bluetooth.BluetoothDevice
import android.content.IntentFilter
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.content.BroadcastReceiver
import android.content.Context
import com.memebattle.heartoracle.feature.base.scan.domain.model.RecyclerDevices
import io.reactivex.Single
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList


class BluetoothConnectService {

    val app = App.instance
    private lateinit var  nameDevicesCallback: BaseCallback<ArrayList<RecyclerDevices>>
    private val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    private val uuid = UUID.fromString("056776ca-8ff1-11e8-9eb6-529269fb1459")
    private var btDevicesList = arrayListOf<RecyclerDevices>()
    private var nameDevicesList = arrayListOf<String>()
    lateinit var socket: BluetoothSocket
    private val deviceFoundReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                if (nameDevicesList.contains(device.name)) {
                    btDevicesList[nameDevicesList.indexOf(device.name)] = RecyclerDevices(device.name, device.address, device)
                } else {
                    btDevicesList.add(RecyclerDevices(device.name, device.address, device))
                    nameDevicesList.add(device.name)
                }
                nameDevicesCallback.onSuccess(btDevicesList)
            }

        }
    }

    fun searchDevice(callback: BaseCallback<ArrayList<RecyclerDevices>>) {
        nameDevicesCallback = callback
        app.registerReceiver(deviceFoundReceiver, IntentFilter(BluetoothDevice.ACTION_FOUND))
        bluetoothAdapter.startDiscovery()
    }

    fun connect(device: BluetoothDevice, baseCallback: BaseCallback<BluetoothSocket>) {
        bluetoothAdapter.cancelDiscovery()
        try {
            socket = device.createRfcommSocketToServiceRecord(uuid)

        } catch (e: IOException) {

        }
        Single.create { emitter: SingleEmitter<BluetoothSocket> ->
            try {
                socket.connect()
                if (socket.isConnected)
                    emitter.onSuccess(socket)
            } catch (connectException: IOException) {
                try {
                    socket.close()
                    emitter.onError(connectException)
                } catch (closeException: IOException) {

                }

            }
        }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : SingleObserver<BluetoothSocket> {
            override fun onSubscribe(d: Disposable) {

            }

            override fun onSuccess(bluetoothSocket: BluetoothSocket) {

            }

            override fun onError(e: Throwable) {

            }
        })
    }


}