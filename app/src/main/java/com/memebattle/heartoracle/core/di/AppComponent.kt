package com.memebattle.heartoracle.core.di

import com.memebattle.heartoracle.core.di.module.BluetoothModule
import com.memebattle.heartoracle.feature.base.scan.presentation.ScanFragment
import com.memebattle.heartoracle.feature.base.scan.presentation.ScanViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [BluetoothModule::class])
interface AppComponent {
    fun inject(scanViewModel: ScanViewModel)
}