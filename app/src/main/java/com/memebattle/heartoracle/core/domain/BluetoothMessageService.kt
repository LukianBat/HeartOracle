package com.memebattle.heartoracle.core.domain

import android.bluetooth.BluetoothSocket
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import java.io.InputStream

class BluetoothMessageService {
    companion object {
        fun getMessage(socket: BluetoothSocket) {
            lateinit var inputStream: InputStream
            try {
                inputStream = socket.inputStream
            } catch (e: IOException) {
                try {
                    socket.close()
                } catch (e1: IOException) {
                    e1.printStackTrace()
                }
            }
            Single.create(SingleOnSubscribe<String> {
                val mmBuffer = ByteArray(1024)
                var numBytes: Int
                while (true) {
                    if (socket.isConnected) {
                        try {
                            numBytes = inputStream.read(mmBuffer)
                            if (numBytes > 0) {
                                var strmsg = String(mmBuffer)
                                strmsg = strmsg.substring(0, numBytes)
                                it.onSuccess(strmsg)
                            }
                        } catch (e: IOException) {
                            socket.close()
                            break
                        }

                    } else {
                        socket.close()
                    }
                }
            }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : SingleObserver<Any> {
                override fun onSuccess(t: Any) {

                }

                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                }

            })
        }
    }
}