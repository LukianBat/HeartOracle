package com.memebattle.heartoracle.core.domain

import android.bluetooth.BluetoothAdapter
import android.content.pm.PackageManager
import com.memebattle.heartoracle.App
import java.io.IOException

class BluetoothService() {

    companion object {
        fun turnOff(baseCallback: EmptyCallback) {
            try {
                BluetoothAdapter.getDefaultAdapter().disable()
                baseCallback.onSuccess()
            } catch (e: IOException) {
                baseCallback.onError(e)
            }
        }

        fun isEnabled(app: App): Boolean {
            return app.packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH)
        }

        fun turnOn(baseCallback: EmptyCallback) {
            try {
                BluetoothAdapter.getDefaultAdapter().enable()
                baseCallback.onSuccess()
            } catch (e: IOException) {
                baseCallback.onError(e)
            }
        }

    }

}