package com.memebattle.heartoracle.core.di.module

import android.bluetooth.BluetoothAdapter
import com.memebattle.heartoracle.App
import com.memebattle.heartoracle.core.domain.BluetoothConnectService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class BluetoothModule {
    @Provides
    @Singleton
    fun provideBluetoothConnectService(): BluetoothConnectService {
        return BluetoothConnectService()
    }
}